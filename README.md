# Yet Another Call Blocker data

This repo contains the "main" phone number database. It is updated once in a couple of months. **The app receives daily (incremental) updates directly [from third-party services](https://gitlab.com/xynngh/YetAnotherCallBlocker#privacy)**.
